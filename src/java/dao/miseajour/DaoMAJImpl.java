package dao.miseajour;

import java.io.Serializable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class DaoMAJImpl  implements DaoMAJ, Serializable{
   
    @PersistenceContext
    private EntityManager em; 
 
    @Override
    public void enregistrerEntite( Object entite) { em.persist(entite); }
    
    @Override
    public void supprimerEntite( Object entite) {
        
       Object asupp=em.merge(entite);
       em.remove(asupp);
    }
   
    @Override
    public void repercuterMAJ( Object entite) { em.merge(entite); }
    
    @Override
    public void detache( Object entite) { em.detach(entite);}
     
    @Override
    public void rafraichir( Object entite) {em.refresh(entite);}
}