
package dao.consultation.region;

import entites.Region;
import java.util.List;

public interface DaoRegion {

    List<Region>  getToutesLesRegions();
}
