
package bal;

import dao.consultation.commande.DaoCommande;
import entites.Commande;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

@Alternative
public class BalImplJPQL implements Bal
{

    @Inject private DaoCommande daoCommande;
    
    @Override
    public Float caAnnuel(int pAnnee) {
        
        Float ca=0f;
        
        for(Commande cmd : daoCommande.getLesCommandes(pAnnee,"R")){
        
            ca+=cmd.montantCommandeHT();
        }
        
        return ca;
    }
  
    @Override
    public Float caMensuel(int pAnnee, int pMois) {
        
         
        Float ca=0f;
        
         for(Commande cmd : daoCommande.getLesCommandes(pAnnee,pMois,"R")){
        
            ca+=cmd.montantCommandeHT();
        }
        
        return ca;
        
    }
 
}
